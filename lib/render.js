/**
 * Render dependencies.
 */
const views = require('koa-views');
var hbs = require('koa-hbs');
const path = require('path');

// setup views mapping .hbs
// to the handlebars template engine
module.exports = views(
  path.join(__dirname, '/../views'), 
  {
    map: { hbs: 'handlebars' },
    extension: 'hbs'
  }
);
