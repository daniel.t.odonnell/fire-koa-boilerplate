const router = require('koa-router')();
const posts = require('../db/posts');

// route definitions
const routes = router
  .get('/posts/new', add)
  .get('/posts/:id/edit', edit)
  .get('/posts/:id', show)
  .get('/', list)
  .post('/posts/:id', update)
  .post('/posts', create)
  .post('/posts/:id/delete', remove)
  
/**
 * Post listing.
 */
async function list(ctx) {
  await ctx.render('posts/list', { posts: await posts.getPosts() }); 
}

/**
 * Add listing.
 */
async function add(ctx) {
  await ctx.render('posts/new');
}

/**
 * Show post :id.
 */
async function show(ctx) {
  let post = await posts.getPost(ctx.params.id);  
  //check for property, as it will return a doc id, even if it's not there!
  if (!post.title) ctx.throw(404, 'invalid post id');
  await ctx.render('posts/show', { post });
}

/**
 * Edit post :id.
 */
async function edit(ctx) {
  let post = await posts.getPost(ctx.params.id);  
  //check for property, as it will return a doc id, even if it's not there!
  if (!post.title) ctx.throw(404, 'invalid post id');
  await ctx.render('posts/edit', { post });
}

/**
 * Create a post.
 */
async function create(ctx) {
  const post = ctx.request.body;
  let id = await posts.createPost(post);
  ctx.redirect(`/posts/${id}`);
}

/**
 * Update a post.
 */
async function update(ctx) {
  const post = { ...ctx.request.body, ...{id : ctx.params.id}};
  await posts.updatePost(post);
  ctx.redirect('/');
}

/**
 * Delete post :id.
 */
async function remove(ctx) {
  let id = await posts.deletePost(ctx.params.id);  
  ctx.redirect('/');
}

module.exports = { routes, list, add, edit, show, create, update, remove}