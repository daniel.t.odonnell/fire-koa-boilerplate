const router = require('koa-router')();
const posts = require('../db/posts');

// route definitions
const routes = router
  .get('/api/posts/:id', show)
  .get('/api/posts', list)
  .post('/api/posts/:id', update)
  .post('/api/posts', create)
  .post('/api/posts/:id/delete', remove)

/**
 * Post listing.
 */
async function list(ctx) {
  ctx.body = await posts.getPosts();
}

/**
 * Show post :id.
 */
async function show(ctx) {
  ctx.body = await posts.getPost(ctx.params.id);
}

/**
 * Create a post.
 */
async function create(ctx) {
  const post = ctx.request.body;
  let id = await posts.createPost(post);
  // return this post
  ctx.params.id = id;
  await show(ctx);
}

/**
 * Update a post.
 */
async function update(ctx) {
  console.log("update.api")
  const post = { ...ctx.request.body, ...{id : ctx.params.id}};
  await posts.updatePost(post);
  await show(ctx);
}

/**
 * Delete post :id.
 */
async function remove(ctx) {
  ctx.body = await posts.deletePost(ctx.params.id);
}

module.exports = { routes, list, show, create, update }