/* FIRE BASE */
const admin = require("firebase-admin");
const serviceAccount = require("./../_private_/boilingplastic-firebase-adminsdk-txhe0-aa89e0132f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://boilingplastic.firebaseio.com"
});

// "database"
exports.db = admin.firestore();