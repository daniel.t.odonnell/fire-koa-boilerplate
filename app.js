
const logger = require('koa-logger');
const router = require('koa-router')();
const koaBody = require('koa-body');

const render = require('./lib/render');

const Koa = require('koa');
const app = module.exports = new Koa();

// middleware
app.use(logger());
app.use(render);
app.use(koaBody());

// load libraries
const posts = require("./lib/posts");
const postsApi = require("./lib/posts.api");

// route definitions
app.use(posts.routes.routes());
app.use(postsApi.routes.routes());


// listen, do you smell something?
if (!module.parent) app.listen(3000);
