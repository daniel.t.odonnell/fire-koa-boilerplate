const { db } = require('../lib/firebase');

exports.getPosts = () => {
  return new Promise(resolve => {
    let data = [];
    db.collection('posts').orderBy('created', 'desc').get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          //console.log(doc.id, '=>', doc.data());
          data.push({...doc.data(), ...{id:doc.id}});
        });
        resolve(data)
      })
      .catch((err) => {
        console.log('Error getting documents', err);
      });
  });
}
  
exports.getPost = (id) => {
  return new Promise(resolve => {
    let data;
    db.collection('posts')
      .doc(id)
      .get()
      .then((doc) => {
        //console.log(doc.id, '=>', doc.data());
        data = {...doc.data(), ...{id:doc.id}};
        resolve(data)
      })
      .catch((err) => {
        console.log('Error getting document', err);
      });
  });
}

exports.createPost = (post) => {
  return new Promise(resolve => {
    post.created = new Date().valueOf();
    let doc = db.collection("posts").doc()
    doc.set(post)
    .then(() => {
      resolve(doc.id)
    })
  })
}

exports.updatePost = (post) => {
  return new Promise(resolve => {
    post.created = new Date().valueOf();
    db.collection("posts").doc(post.id).set(post)
    .then(() => {
      resolve(post.id)
    })
  })
}

exports.deletePost = (id) => {
  return new Promise(resolve => {
    let data;
    db.collection('posts')
      .doc(id)
      .delete()
      .then(() => {resolve( { id } )} )
      .catch((err) => {
        console.log('Error deleting document', err);
      });
  });
}